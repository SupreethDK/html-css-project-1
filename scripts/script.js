
let count = 0;
function handleClick() {
    count++;
    console.log(count);
    if(count%2 !== 0) {
        document.querySelector('.price-list-1').innerHTML = '$19.99';
        document.querySelector('.price-list-2').innerHTML = '$24.99';
        document.querySelector('.price-list-3').innerHTML = '$39.99';
    } else {
        document.querySelector('.price-list-1').innerHTML = '$199.99';
        document.querySelector('.price-list-2').innerHTML = '$249.99';
        document.querySelector('.price-list-3').innerHTML = '$399.99';
    }
}

